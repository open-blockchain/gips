==Garlicoin’s Issues, an Explanation and our Solutions==

Recently there has been a lot of misinformation flying around that I will attempt to dispel and explain in this post. We are being open and honest with the community about what is happening, though I understand your suspicion — given the evidence you have, the obvious conclusion is that we are dishonest.

We are not dishonest, we are not thieves, and I will try to make that clear to you.

The misinformation seems to be based on three things: The missing 40k GRLC, the supposed “ASIC” on the network and the ludicrous idea that we are attempting to cover up the “ASIC” and 40k. I will briefly explain the situation with both, then try to clear up the misunderstanding.

==THE 40K==

The 40k refers to around 40 thousand Garlicoin that were premined. These funds were supposed to be sent to the airdrop fund, but as the airdrop drew close it became clear to us that the identity of the wallet owner wasn’t known and we did not have control over those funds. I will reiterate our apology — we were naive and didn’t consider the possibility of members of our dev team being malicious. For that we can only apologise, and do everything we can to fix our mistake.

Unfortunately, we as developers are somewhat powerless to enact a ban on the coins. After talking with pool operators, they have made it clear they will not support a fork to ban the 40k. We as developers cannot force them to run our code, nor can we fork without support from the pool owners.

There is also the consideration of the risks of a ban, and ultimately they could cause more damage to Garlicoin than allowing the 40k to remain in circulation. We as developers must do what is best for the coin, irrespective of damage to our own reputation.

===THERE WILL BE NO FORK TO BAN THE COINS.===

We have been tracing these coins, and trying to determine their owner. Due to our inability to ban them, all we can do is keep you updated and be completely transparent.

==THE SUPPOSED ASIC==

There have also been rumours that an ASIC is active on our network. We investigated this risk and determined it to be unlikely.

However, as part of our investigation we made it clear to the community that we were looking into reports of an ASIC. Due to poor communication on our part, for which I apologise, and rumours of scryptN ASIC development from several years prior the myth perpetuated that ASICs were active on our network. This is unlikely, and the hashrate the scryptN ASIC would’ve been able to get is very small — in fact it is significantly lower than what could be achieved with cloud computing servers.

Despite the unlikeliness of ASIC usage, we need to take this risk seriously — any risk to the fairness of the network must be thoroughly investigated and action must be taken if necessary.

As a result I am developing the first Garlicoin Improvement Proposal (GIP). GIP-1 is currently being drafted and the implementation is being worked out — should the community show support for it, we as developers will do everything in our power to activate GIP-1 on the network.
GIP-1 will, put simply, be a hard fork to a new Proof of Work algorithm. There is a high possibility this will be Lyra2REv2, however other interesting developments in ASIC resistant proof of work — such as Ravencoin — could be implemented.

===NOTE: THIS PROPOSED HARD FORK WOULD NOT RISK YOUR FUNDS — IT MAY NOT EVEN BE ACTIVATED===

==TRANSPARENCY==

We are being completely open about our current issues, and the steps we are making to fix them. Some members of the community are claiming we are censoring information about the two problems I detailed above. As this post has made clear, that suggestion is completely false. We have always tried to include the community as much as possible, and the current events are no exception.
THE FUTURE

Despite the current problems, we as a development team are looking to the future. We intend this coin to be not only a fun and friendly introduction to cryptocurrency, but also a worthwhile coin in its own right.

We will be posting a roadmap shortly, and we intend to stay up to date with the latest developments in the cryptocurrency space, including new and exciting scaling proposals, confidential transactions, continued ASIC resistance and better blockchain storage methods to help syncing and operation of full nodes.

===Garlicoin will be a coin for the future.===

Retosen.

